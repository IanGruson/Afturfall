# Language 
GDScript

# Quel type de jeu ?
- 2D 
- RPG 
- Solo

# Art kit ou notre propre art kit ? 
Bountiful Bits by VEXED : https://v3x3d.itch.io/bountiful-bits
Bit Bonanza by VEXED : https://v3x3d.itch.io/bit-bonanza

# Scénario ? 
Concept du personnage avec un système d'auto guide/ou de quelque chose
permettant de le rendre plus fort que l'humain lambda.  Le personnage commence
dans notre ère actuelle (XXIème siècle), puis se fait téléporter dans un monde
d'heroic/fantasy. Le monde possède son propre lore, ses propres intrigues politiques (par
rapport aux royaumes), son propre fonctionnement (par exemple la chasse aux
monstres est devenu monnaie courante, pour cela il faut engager ce qu'on appel
des "aventuriers" qui sont des chasseurs de monstres ou des mercenaires classés
du rang D à SS).

Lore : Monstre basique vu dans les différentes mythologie (Orc, haut-elfe,
dryade, loup-garou, etc..) Royaume : Structure différentes pour chaque
