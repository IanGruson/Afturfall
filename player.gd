extends Area2D
var screen_size
var scale_factor = 5
var tile_size = 10
var inputs = {"move_right": Vector2.RIGHT,
			"move_left": Vector2.LEFT,
			"move_up": Vector2.UP,
			"move_down": Vector2.DOWN,
			"move_up_left": Vector2.UP + Vector2.LEFT,
			"move_up_right": Vector2.UP + Vector2.RIGHT,
			"move_down_left": Vector2.DOWN + Vector2.LEFT,
			"move_down_right": Vector2.DOWN + Vector2.RIGHT }
			
onready var ray = $RayCast2D


# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport().size
	position = position.snapped(Vector2.ONE * tile_size * scale_factor)
	position += Vector2.ONE * tile_size * scale_factor/2
#	$Sprite.size.x = screen_size.x * 0.10
#	$Sprite.size.y = screen_size.y * 0.10
	$Sprite.scale = Vector2(scale_factor,scale_factor)

	pass # Replace with function body.
func _unhandled_input(event):
	for dir in inputs.keys():
		if event.is_action_pressed(dir):
			move(dir)

func move(dir):
	ray.cast_to = inputs[dir] * (tile_size * scale_factor)
	ray.force_raycast_update()
	if !ray.is_colliding():
		position += inputs[dir] * (tile_size * scale_factor)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
